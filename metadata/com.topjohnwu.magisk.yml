AntiFeatures:
  - NonFreeAdd
Categories:
  - System
License: GPL-3.0-or-later
AuthorName: John Wu
SourceCode: https://github.com/topjohnwu/Magisk
IssueTracker: https://github.com/topjohnwu/Magisk/issues
Changelog: https://topjohnwu.github.io/Magisk/changes.html
Donate: https://paypal.me/magiskdonate

AutoName: Magisk
Description: |-
    Magisk is a suite of open source software for customizing Android, supporting devices higher than Android 5.0.
    Some highlight features:
    * MagiskSU: Provide root access for applications
    * Magisk Modules: Modify read-only partitions by installing modules
    * MagiskBoot: The most complete tool for unpacking and repacking Android boot images
    * Zygisk: Run code in every Android applications' processes

RequiresRoot: 'yes'

RepoType: git
Repo: https://github.com/topjohnwu/Magisk

Builds:
  - versionName: '1.0'
    versionCode: 1
    commit: cacf8736454b9c586a0b9f0af86451f6c87e200a
    subdir: stub
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y openjdk-11-jdk-headless
      - update-alternatives --auto java
    gradle:
      - yes
    rm:
      - native
      - tools
    prebuild:
      - echo -e 'version=25.0' > ../config.prop
      - sed -i -e 's|"https://.*"|"https://f-droid.org/repo/com.topjohnwu.magisk_${Config.versionCode}.apk"|'
        -e '/versionCode/s/1/$$VERCODE$$/' build.gradle.kts
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java

  - versionName: '22.0'
    versionCode: 22000
    commit: v22.0
    submodules: true
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y g++
      - pushd build/srclib/cpython
      - ./configure
      - make -j`nproc`
      - make altinstall
      - popd
    output: app/build/outputs/apk/release/app-release-unsigned.apk
    srclibs:
      - cpython@v3.9.2
    prebuild:
      - echo -e 'version=$$VERSION$$' > ./config.prop
      - sed -i -e '/buildTypes/,+11d' ./build.gradle.kts
      - sed -i -e 's/ndkPath.*/ndkPath = "${System.getenv("ANDROID_NDK_HOME")}"/'
        ./build.gradle.kts
      - sed -i -e '/android.ndkPath/d' -e '/ndk.dir/d' ./local.properties
      - sed -i -e 's/21d/21e/' -e 's/21.3.6528147/21.4.7075529/' ./gradle.properties
      - sed -i -e "s/op.join(ndk_root, 'magisk')/os.environ['ANDROID_NDK_HOME']/"
        ./build.py
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java
    scandelete:
      - native/jni/external
      - tools
    build:
      - ln -s `which gradle` ./gradlew
      - python3.9 ./build.py -rv stub
      - mv stub/build/outputs/apk/release/stub-release-unsigned.apk out/stub-release.apk
      - python3.9 ./build.py -rv binary
      - python3.9 ./build.py -rv app
    ndk: r21e

  - versionName: '22.1'
    versionCode: 22100
    commit: 632cee1613eecae03b68f2b3d8ab1358a1b5bf4a
    submodules: true
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y g++
      - pushd build/srclib/cpython
      - ./configure
      - make -j`nproc`
      - make altinstall
      - popd
    output: app/build/outputs/apk/release/app-release-unsigned.apk
    srclibs:
      - cpython@v3.9.4
    prebuild:
      - echo -e 'version=$$VERSION$$' > ./config.prop
      - sed -i -e '/buildTypes/,+11d' ./build.gradle.kts
      - sed -i -e 's/ndkPath.*/ndkPath = "${System.getenv("ANDROID_NDK_HOME")}"/'
        ./build.gradle.kts
      - sed -i -e '/android.ndkPath/d' -e '/ndk.dir/d' ./local.properties
      - sed -i -e "s/op.join(ndk_root, 'magisk')/os.environ['ANDROID_NDK_HOME']/"
        ./build.py
      - sed -i -e 's/Key.CHECK_UPDATES, true/Key.CHECK_UPDATES, false/' ./app/src/main/java/com/topjohnwu/magisk/core/Config.kt
      - sed -i -e '/pid_t getsid/,+14d' native/jni/external/busybox/libbb/missing_syscalls.c
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java
    scandelete:
      - native/jni/external
      - tools
    build:
      - ln -s `which gradle` ./gradlew
      - python3.9 ./build.py -rv stub
      - mv stub/build/outputs/apk/release/stub-release-unsigned.apk out/stub-release.apk
      - python3.9 ./build.py -rv binary
      - python3.9 ./build.py -rv app
    ndk: r21e

  - versionName: '23.0'
    versionCode: 23000
    commit: v23.0
    submodules: true
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y g++
      - pushd build/srclib/cpython
      - ./configure
      - make -j`nproc`
      - make altinstall
      - popd
    output: app/build/outputs/apk/release/app-release-unsigned.apk
    srclibs:
      - cpython@v3.9.5
      - vboot@release-R91-13904.B
    prebuild:
      - echo -e 'version=$$VERSION$$' > ./config.prop
      - sed -i -e 's/versionCode=.\+/versionCode=$$VERCODE$$/' gradle.properties
      - sed -i -e '/buildTypes/,+11d' ./build.gradle.kts
      - sed -i -e 's/ndkPath.*/ndkPath = "${System.getenv("ANDROID_NDK_HOME")}"/'
        ./build.gradle.kts
      - sed -i -e '/android.ndkPath/d' -e '/ndk.dir/d' ./local.properties
      - sed -i -e "s/op.join(ndk_root, 'magisk')/os.environ['ANDROID_NDK_HOME']/"
        ./build.py
      - sed -i -e 's/Key.CHECK_UPDATES, true/Key.CHECK_UPDATES, false/' ./app/src/main/java/com/topjohnwu/magisk/core/Config.kt
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java
    scandelete:
      - native/jni/external
      - tools
    build:
      - pushd $$vboot$$
      - make ARCH=arm futil
      - popd
      - cp $$vboot$$/build/futility/futility tools
      - ln -s `which gradle` ./gradlew
      - python3.9 ./build.py -rv stub
      - mv stub/build/outputs/apk/release/stub-release-unsigned.apk out/stub-release.apk
      - python3.9 ./build.py -rv binary
      - python3.9 ./build.py -rv app
    ndk: r21e

  - versionName: '24.1'
    versionCode: 24100
    commit: 32fc34f9226743c2b0be75c0c3bc427031305464
    submodules: true
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y g++
      - apt-get install -y openjdk-11-jdk-headless
      - update-alternatives --auto java
      - pushd build/srclib/cpython
      - ./configure
      - make -j$(nproc)
      - make altinstall
      - popd
    output: app/build/outputs/apk/release/app-release-unsigned.apk
    srclibs:
      - cpython@v3.9.10
      - vboot@release-R91-13904.B
    prebuild:
      - echo -e 'version=$$VERSION$$' > ./config.prop
      - sed -i -e '/buildTypes/,+11d' -e 's/ndkPath.*/ndkPath = "${System.getenv("ANDROID_NDK_HOME")}"/'
        buildSrc/src/main/java/Setup.kt
      - sed -i -e '/android.ndkPath/d' -e '/ndk.dir/d' ./local.properties
      - sed -i -e "s/op.join(ndk_root, 'magisk')/os.environ['ANDROID_NDK_HOME']/"
        ./build.py
      - sed -i -e 's/ missing.cpp//' native/jni/utils/Android.mk
      - sed -i -e 's/Key.CHECK_UPDATES, true/Key.CHECK_UPDATES, false/' ./app/src/main/java/com/topjohnwu/magisk/core/Config.kt
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java
    scandelete:
      - native/jni/external
      - tools
    build:
      - pushd $$vboot$$
      - make ARCH=arm futil
      - popd
      - cp $$vboot$$/build/futility/futility tools
      - ln -s $(which gradle) ./gradlew
      - python3.9 ./build.py -rv stub
      - mv stub/build/outputs/apk/release/stub-release-unsigned.apk out/stub-release.apk
      - python3.9 ./build.py -rv binary
      - python3.9 ./build.py -rv app
    ndk: r21e

  - versionName: '24.3'
    versionCode: 24300
    commit: v24.3
    submodules: true
    sudo:
      - apt-get update || apt-get update
      - apt-get install -y g++
      - apt-get install -y openjdk-11-jdk-headless
      - update-alternatives --auto java
      - pushd build/srclib/cpython
      - ./configure
      - make -j$(nproc)
      - make altinstall
      - popd
    output: app/build/outputs/apk/release/app-release-unsigned.apk
    srclibs:
      - cpython@v3.9.10
      - vboot@release-R91-13904.B
    prebuild:
      - echo -e 'version=$$VERSION$$' > ./config.prop
      - sed -i -e '/buildTypes/,+11d' -e 's/ndkPath.*/ndkPath = "${System.getenv("ANDROID_NDK_HOME")}"/'
        buildSrc/src/main/java/Setup.kt
      - sed -i -e '/android.ndkPath/d' -e '/ndk.dir/d' ./local.properties
      - sed -i -e "s/op.join(ndk_root, 'magisk')/os.environ['ANDROID_NDK_HOME']/"
        ./build.py
      - sed -i -e '/compat.cpp/d' native/jni/utils/Android.mk
      - sed -i -e 's/Key.CHECK_UPDATES, true/Key.CHECK_UPDATES, false/' ./app/src/main/java/com/topjohnwu/magisk/core/Config.kt
    scanignore:
      - app/shared/src/main/java/com/topjohnwu/magisk/utils/DynamicClassLoader.java
    scandelete:
      - native/jni/external
      - tools
    build:
      - pushd $$vboot$$
      - make ARCH=arm futil
      - popd
      - cp $$vboot$$/build/futility/futility tools
      - ln -s $(which gradle) ./gradlew
      - python3.9 ./build.py -rv stub
      - mv stub/build/outputs/apk/release/stub-release-unsigned.apk out/stub-release.apk
      - python3.9 ./build.py -rv binary
      - python3.9 ./build.py -rv app
    ndk: r23b

MaintainerNotes: |-
    22.1: https://github.com/topjohnwu/ndk-busybox/commit/31b2b5780bf9fe982e1f3d9df0bd4b1b44d6e394
    in submodule is removed for some unkonwn reasons. Re-add it to fix the multiple definition error.

    The stub apk is built as a standalone apk first. It goes into the archive directly. It's downloaded
    from f-droid.org later when building the corresponding main apk. The stub apk needs to be added
    first and the main apk should be built in the next build cycle.

AutoUpdateMode: None
UpdateCheckMode: HTTP
UpdateCheckData: https://topjohnwu.github.io/magisk-files/stable.json|"versionCode":\s"(\d+)"|.|"version":\s"([\d.]+)"
CurrentVersion: '24.3'
CurrentVersionCode: 24300
